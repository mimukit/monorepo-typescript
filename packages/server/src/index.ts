// tslint:disable-next-line:no-var-requires
require('dotenv-safe').config();
import server from './server';

const port = process.env.PORT || 4000;

server.listen({ port }, () =>
  process.env.NODE_ENV === 'production'
    ? console.info(`Server started on port ${port}`)
    : console.info(`Server started at http://localhost:${port}/graphql`),
);
