import React, { Component } from 'react';
import './App.css';

import msg from '@monorepo/common';

class App extends Component<{ title: string }, {}> {
  render() {
    return (
      <div className="App">
        <h1>{this.props.title}</h1>
        <h4>
          Message from common package: <span className="red-text">{msg}</span>
        </h4>
      </div>
    );
  }
}

export default App;
