#####################
## Build Server Code
#####################

FROM node:10-alpine AS ServerBuild
WORKDIR /usr/src/app

COPY package.json .
COPY yarn.lock .
COPY lerna.json .
COPY tsconfig.json .

COPY ./packages/common/package.json ./packages/common/
COPY ./packages/server/package.json ./packages/server/

RUN yarn install

COPY ./packages/common/src/ ./packages/common/src/
COPY ./packages/common/tsconfig.json ./packages/common/

COPY ./packages/server/src/ ./packages/server/src/
COPY ./packages/server/tsconfig.json ./packages/server/

RUN yarn build:backend

#############################
## Production Image
#############################

FROM node:10-alpine AS ServerProd

ENV NODE_ENV production

WORKDIR /usr/src/app

COPY package.json .
COPY yarn.lock .

COPY ./packages/common/package.json ./packages/common/
COPY ./packages/server/package.json ./packages/server/

RUN yarn install --production

COPY --from=ServerBuild /usr/src/app/packages/common/build ./packages/common/build/
COPY --from=ServerBuild /usr/src/app/packages/server/build ./packages/server/build/
COPY ./packages/server/.env.example ./packages/server/

WORKDIR /usr/src/app/packages/server

CMD node ./build/index.js

EXPOSE 4000
EXPOSE $PORT
